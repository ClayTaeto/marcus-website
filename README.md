# README #

This website was created to be my personal site. 

### What is this repository for? ###

* Quick and dirty code sample
* Display competency of several web technologies. 
* personal blog actually comes as a third priority. 

### What is it actually using? ###

* MongoDB hosted at MongoHQ
* Node.js for the webserver
* Knockout for frontend templating
* Express for dem restful APIs
* Bootstrap for responsive elements