function isDone(){

if(!window.Derp){window.Derp = 0}
  window.Derp += 1;
  console.log("isDone");
  console.log(window.Derp)
  if (window.Derp == 2){
    console.log("ChickenDinner");
    $(".loading").hide();
    $("#featured2").attr("data-orbit", "");
    $(document).foundation();

  }
}

function FeedView(){
var self = this;
  //$.get()
  //{title: , tags: , id: , imgs: [], message:   }
  self.FeedItem = ko.observableArray()
  self.HeroUnit = ko.observableArray()

  $.get("/api/feed/tags/Announcements?limit=5")
    .done(function(data) {
      self.HeroUnit(data.posts)
    })
    .fail(function(data) {
      self.HeroUnit(mockAnnouncements.posts)
    })

  $.get("/api/feed?limit=4&casual=true")
    .done(function(data) {
      self.FeedItem(data.posts)
    })
    .fail(function(){
      self.FeedItem(mockData)
    })

  }
var Feed = new FeedView()
//FeedContainer
ko.applyBindings(Feed);


var mockData =   [
    {title: "Bacon Jam entry!", tags: ["Announcement", "Games", "Release"], id: "Foo", thumb: "http://placehold.it/250x250&text=Zombies!", message: "Note to self: Write article" },
    {title: "Project with spiders?", tags: "Foo", id: "Foo", thumb: "http://placehold.it/250x250&text=Community", message:  "Note to self: Write article" },
    {title: "New Game, 20XX", tags: "Foo", id: "Foo", thumb: "http://placehold.it/250x250&text=Game!, In space!", message:  "Note to self: Write article" },
    {title: "New Game, 20XX", tags: "Foo", id: "Foo", thumb: "http://placehold.it/250x250&text=Game!, In space!", message:  "Note to self: Write article" }
  ];

var mockAnnouncements = {"tags":"Announcements","search":{"limit":"5"},"posts":[{"title":"Messing with Unity","tags":["Announcements","Games"],"HeroUnit":"/images/MessingWithUnity.png","thumb":"/images/MessingWithUnityThumb.png","message":"<p>I  have a free trial of Unity3d Pro and I'd thought I'd give it a whirl. Right off the bat, I've got to say that the Physics on unity are pretty great. A few clicks and I've got blocks colliding correctly and a character walking around.</p> <p>Getting guns and explosives working was a different story. At one point I had the explosion force continuously exploding! It made the blocks around it dance if there was more then 2 grenades in the area. </p>","_id":"51c8c451dbdcce101d000001"},{"title":"BaconJam05 Entry","tags":["Announcements","Games","Releases"],"HeroUnit":"/images/AttackOnCampFirewood.png","thumb":"/images/zombie.png","message":"<p>I entered the fifth BaconJam this weekend. For those who are unfamiliar, it's a coding competition where the contestants make games in only 48 hours. I teamed up with Senshaij this time around and hopefully our entry will do well.  Needless to say i'm quite burnt out because of it, but I am quite proud to say I made enough of a game to submit an entry! This time around the theme was \"Lights out\" so naturally I made a survival zombie game. Go ahead and <a href='https://dl.dropboxusercontent.com/u/59593311/Releases/Attack%20on%20Camp%20Firewood/index.html'>try it out.</a></p> <p>In the game, the tables are turned. You control the monsters and it's the NPC's that have to fight to survive! It's played by using your mouse to select which monsters to spawn and then ordering them around RTS style. The selection highlight bugged out at the last minute, so I through a bit of partical effects as a makeshift visual representation of what got selected.</p> <p>After you select a few monsters, you can order them to move by right clicking somewhere on the map. They'll pathfind to the location and damage anything in thier range. Unfortunately Attack orders aren't finish so the monsters will not move to attack on their own.</p> <p>As for the competition, I've found a few games that where really good. I've played through \"Occupy Vampire Wallstreet\" and \"Eat the Darkness\" quite a few times. Overall I'm pretty optimistic. I think I'll rank higher then 50th place. </p> ","_id":"51c89840e95cc8d026000001"},{"title":"\"New Game, 20XX\"","tags":[" Games","Release","Announcements"],"HeroUnit":"/images/20XX.png","thumb":"/images/20XXThumb.png","message":"<p>I was on a certain subforum on reddit the other day and everyone there was contemplating whether or not a game was going to be actually worked on or not. That's when I decided to start this project. <a href='https://dl.dropboxusercontent.com/u/59593311/Releases/20XX/20XX%20vr0.0002.zip'> This is what I have so far.</a></p> <p> It's a space ship that navigates with physics with a planet and a moon. The thrusters' position actually denotes where the force gets applied onto the ship in order to move it. Best part is that when you parent the ship to a thruster, it sets up basically automatically. The down side is I couldn't just throw thrusters on a block and expect it to fly. Ships navigate with physics and without a ship editor written, symmetry has been a bitch.</p> <p>  As for the planets, They're really big and they are not the same size. It may not look like it, but the red planet is 1000 times larger then it's moon. I'm not sure if that's a realistic satellite relationship, but I haven't programmed gravity in yet so it'll do. I also tried landing on it's moon, which I don't recommend in this build. Without gravity it feels weird. As soon as you touch down you get spun pretty bad. There is no rotational boosters on the ship yet, so stabilizing it was quite interesting. I'm getting dizy just thinking about it. </p> <p>Gravity has been quite elusive. It's easy to just apply a force in a general direction but to have force relative to celestial bodies? I'm doing some research on that. So far I've figured out the formula for gravitational force and gravitational fields. The tricky part is implementing them. I'll let you know how it's going.</p> ","_id":"51c8c7afdbdcce101d000002"}]}